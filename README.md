Déposez ici vos scènes ressources permettant de réaliser les controles : par la souris, joystick, etc..

précisez bien dans le nom de quoi il s'agit afin que d'autres puissent les réutiliser


si vous proposez des modifications importantes, créez des branches GIT sur la scène originale, afin de la conserver intacte pour information.


ces scènes peuvent ensuite etre réutilisées dans d'autres jeux.